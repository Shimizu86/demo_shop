from django.contrib import admin
from .models import Cupon


class CuponAdmin(admin.ModelAdmin):
    list_display = ['cupon_code', 'valid_from', 'valid_to', 'discount', 'active']
    list_filter = ['valid_from', 'valid_to', 'active']
    search_fields = ['cupon_code']


admin.site.register(Cupon, CuponAdmin)
