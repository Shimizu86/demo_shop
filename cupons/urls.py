from django.conf.urls import url
from .views import CuponApply

urlpatterns = [
    url(r'^apply', CuponApply, name='apply')
]
