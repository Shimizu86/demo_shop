from celery import task
from django.core.mail import send_mail
from .models import Order


@task
def order_created(order_id):
    """
    Отправка сообщения о создании покупки по электронной почте
    """
    order = Order.objects.get(id=order_id)
    subject = 'Новый заказ с номером {}'.format(order.id)
    message = 'Уважаемый, {}, Выуспешно создали заказ.' \
              'Номер вашего заказа {}'.format(order.first_name, order.id)
    mail_send = send_mail(subject, message, 'shimizuu86@gmail.com', [order.email])
    return mail_send
